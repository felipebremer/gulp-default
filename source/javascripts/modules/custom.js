function custom(options) {
    console.log(options);
    return {
        loadData: function () {
            console.log("loadData");
        },
        init: function () {
            console.log("init");
            this.loadData();
        }
    };
};

module.exports = custom;