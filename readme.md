# GULP DEFAULT

É um projeto pronto de gulp.

- gulp-less
- gulp-clean-css
- gulp-uglify
- gulp-browserify
- gulp-imagemin
- imagemin-jpeg-recompress
- imagemin-pngquant
- gulp-html-minifier
- gulp-html-replace
- gulp-inject-partials
- browser-sync

Pré-requisitos do projeto:

 - 	[Node.js](https://nodejs.org/en/)
 - 	Ruby
    -  [Windows](https://www.ruby-lang.org/pt/documentation/installation/)
    -  [Linux](https://www.ruby-lang.org/pt/documentation/installation/)
 -	[Gulp.js](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)

Execute o comando `npm install` para instalar as dependencias do node

Para executar o gulp execute o comando abaixo no ms-dos ou git bash:

Task default:
```
gulp
```

Watch abrirá o projeto no browser com live-reload:
```
gulp watch
```

Build para produção:
```
gulp --env=prod
```

A tag `<partial>` é removida no build final ao utilizar o `env=prod`, não associe css à ela.

Com o gulp rodando [clique aqui](http://localhost:3000/) para acessar o projeto. (Ao rodar o commando `watch` uma aba abrirá automaticamente)
